package per.sykes;

import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author sykes
 * @Description
 * @Date 2021/12/5 17:21
 **/
public class Test {

    public static void main(String[] args) {
		ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext("applicationContext.xml");
//		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("selfEditor.xml");
         Person bean = applicationContext.getBean(Person.class);
    }

}
