package per.sykes.aop;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import per.sykes.aop.annotation.config.SpringConfiguration;

/**
 * @author sykes
 * @Description
 * @Date 2021/12/28 22:42
 **/
public class Test {

	public static void main(String[] args) {
//		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(SpringConfiguration.class);
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("aop.xml");
		context.refresh();

	}

}
