package per.sykes.aop.annotation.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@Configuration
@ComponentScan("per.sykes.aop.annotation")
@EnableAspectJAutoProxy
public class SpringConfiguration {

}