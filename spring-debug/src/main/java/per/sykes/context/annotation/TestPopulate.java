package per.sykes.context.annotation;

import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author sykes
 * @Description
 * @Date 2021/12/27 0:00
 **/
public class TestPopulate {

	public static void main(String[] args) {
		ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext("populateBean.xml");
	}

}
