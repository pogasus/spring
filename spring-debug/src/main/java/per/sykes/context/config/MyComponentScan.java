package per.sykes.context.config;

import org.springframework.beans.factory.annotation.Lookup;
import org.springframework.context.annotation.*;
import per.sykes.Person;
import per.sykes.PersonService;

/**
 * @author sykes
 * @Description
 * @Date 2021/12/12 10:48
 **/
@Configuration
@Import(PersonService.class)
@ComponentScan("per.sykes")
@DependsOn
public class MyComponentScan {

	@Configuration
	@ComponentScan("per.sykes")
	class Internal {
	}

	@Bean
	@Lookup
	public Person person() {
		return new Person();
	}


}
