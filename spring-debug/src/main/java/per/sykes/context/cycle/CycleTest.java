package per.sykes.context.cycle;

import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * CycleTest
 *
 * @author sykes
 * @date 2021-12-29 9:48
 */
public class CycleTest {

	public static void main(String[] args) {
		final ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("cycle.xml");
	}

}
