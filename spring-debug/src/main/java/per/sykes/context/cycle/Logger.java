package per.sykes.context.cycle;

/**
 * Logger
 *
 * @author sykes
 * @date 2021-12-29 10:56
 */
public class Logger {

	public void recordBefore(){
		System.out.println("recordBefore");
	}

	public void recordAfter(){
		System.out.println("recordAfter");
	}

}
