package per.sykes.context.cycle;

/**
 * TestA
 *
 * @author sykes
 * @date 2021-12-29 10:25
 */
public class TestA {

	private TestB testB;

	public TestB getTestB() {
		return testB;
	}

	public void setTestB(TestB testB) {
		this.testB = testB;
	}
}
