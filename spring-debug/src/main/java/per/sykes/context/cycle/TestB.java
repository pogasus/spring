package per.sykes.context.cycle;

/**
 * TestA
 *
 * @author sykes
 * @date 2021-12-29 10:25
 */
public class TestB {

	private TestC testC;

	public TestC getTestC() {
		return testC;
	}

	public void setTestC(TestC testC) {
		this.testC = testC;
	}

	@Override
	public String toString() {
		return "TestB{" +
				"testC=" + testC +
				'}';
	}
}
