package per.sykes.context.cycle;

/**
 * TestC
 *
 * @author sykes
 * @date 2021-12-29 10:25
 */
public class TestC {

	private TestA testA;

	public TestA getTestA() {
		return testA;
	}

	public void setTestA(TestA testA) {
		this.testA = testA;
	}

	@Override
	public String toString() {
		return "TestC{" +
				"testA=" + testA +
				'}';
	}
}
