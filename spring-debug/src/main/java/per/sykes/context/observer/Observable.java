package per.sykes.context.observer;

/**
 * @author sykes
 * @Description 被观察者
 * @Date 2021/12/14 22:22
 **/
public interface Observable {


	public void addObserver(Observer observer);
	public void deleteObserver(Observer observer);
	public void notifyObserver(String Str);


}
