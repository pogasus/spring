package per.sykes.context.observer;

/**
 * @author sykes
 * @Description
 * @Date 2021/12/14 22:23
 **/
public interface Observer {

	void make(String str);

}
