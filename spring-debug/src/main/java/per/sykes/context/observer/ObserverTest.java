package per.sykes.context.observer;

/**
 * @author sykes
 * @Description
 * @Date 2021/12/14 22:33
 **/
public class ObserverTest {

	public static void main(String[] args) {
		// 创建被观察者
		BadMan badMan = new BadMan();
		// 创建观察者
		PoliceMan policeMan = new PoliceMan();
		PoliceMan2 policeMan2 = new PoliceMan2();
		// 向被观察者中添加观察者
		badMan.addObserver(policeMan);
		badMan.addObserver(policeMan2);
		// 等待罪犯触发行为
		badMan.run();
	}

}
