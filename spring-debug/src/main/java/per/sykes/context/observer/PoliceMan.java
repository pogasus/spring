package per.sykes.context.observer;

/**
 * @author sykes
 * @Description
 * @Date 2021/12/14 22:32
 **/
public class PoliceMan implements Observer{
	@Override
	public void make(String str) {
		System.out.println("开始行动");
		System.out.println("------" + str);
	}
}
