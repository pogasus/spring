package per.sykes.context.observer;

/**
 * @author sykes
 * @Description
 * @Date 2021/12/14 22:32
 **/
public class PoliceMan2 implements Observer {
	@Override
	public void make(String str) {
		System.out.println("PoliceMan2开始行动");
		System.out.println("------" + str);
	}
}
