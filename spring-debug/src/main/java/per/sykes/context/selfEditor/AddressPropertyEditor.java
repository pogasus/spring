package per.sykes.context.selfEditor;

import java.beans.PropertyEditorSupport;

/**
 * @author sykes
 * @Description 自定义属性编辑器
 * @Date 2021/12/9 0:04
 **/
public class AddressPropertyEditor extends PropertyEditorSupport {

	@Override
	public void setAsText(String text) throws IllegalArgumentException {
		String[] spl = text.split("_");
		Address address = new Address();
		address.setProvince(spl[0]);
		address.setCity(spl[1]);
		address.setTown(spl[2]);
		this.setValue(address);
	}
}
