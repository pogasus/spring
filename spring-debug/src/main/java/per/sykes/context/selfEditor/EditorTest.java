package per.sykes.context.selfEditor;

import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author sykes
 * @Description
 * @Date 2021/12/9 0:15
 **/
public class EditorTest {

	public static void main(String[] args) {
		ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext("selfEditor.xml");
		Customer customer = applicationContext.getBean(Customer.class);
		System.out.println(customer);
	}

}
