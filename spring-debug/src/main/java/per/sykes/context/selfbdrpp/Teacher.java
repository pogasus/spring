package per.sykes.context.selfbdrpp;

/**
 * @author sykes
 * @Description
 * @Date 2021/12/10 1:37
 **/
public class Teacher {

	private String name;

	public Teacher() {
		System.out.println("创建teacher对象");
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Teacher{" +
				"name='" + name + '\'' +
				'}';
	}
}
